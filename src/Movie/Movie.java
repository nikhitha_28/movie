package Movie;

public class Movie {
    private String title;
    private String studio;
    private String rating;
    private Movie(String t,String s,String r){
        this.title=t;
        this.studio=s;
        this.rating=r;
    }
    public Movie(String t1,String s1){
        this.title=t1;
        this.studio=s1;
        this.rating="PG";
    }
    public static Movie[] getPg(Movie[] tollywoodMovies){
        Movie[] pgMov = new Movie[tollywoodMovies.length];
        for (int i = 0; i < tollywoodMovies.length; i++) {
            if (tollywoodMovies[i].rating.equals("PG")) {
                pgMov[i] = tollywoodMovies[i];

            }
        }
        return pgMov;
    }


    public static void main(String[] args){
        Movie film=new Movie("Casino Royale","Eon Productions","PG-13");
        Movie film1=new Movie("Movie1","Studio1");
        Movie[] tollywoodMovies=new Movie[3];
        tollywoodMovies[0]=new Movie("Movie1","Studio1","PG");
        tollywoodMovies[1]=new Movie("Movie2","Studio2","PG-13");
        tollywoodMovies[2]=new Movie("Movie3","studio3","PG");

        Movie[] PgMovie=getPg(tollywoodMovies);
        System.out.println(PgMovie.length);

        for (int i=0;i< tollywoodMovies.length;i++){
            if (PgMovie[i]!=null){
            System.out.println(PgMovie[i].title+" "+PgMovie[i].studio+" "+PgMovie[i].rating);}
        }

        //System.out.println(Arrays.asList(PgMovie));

        //System.out.println(film1.title);
        //System.out.println(Arrays.toString(PgMovie));
    }
}
